

from django.conf.urls import url
from . import views

urlpatterns = [

    url(r'^loginuser$', views.loginuser, name='loginuser'),
    url(r'^logoutuser$', views.logoutuser, name='logoutuser'),

    url(r'^landing$', views.landing, name='landing'),
    url(r'^emailformat$', views.emailformat, name='emailformat'),


    url(r'^inputsheet$', views.inputsheet, name='inputsheet'),
    url(r'^post_inputsheet$', views.post_inputsheet, name='post_inputsheet'),
    url(r'^adminapproval$', views.adminapproval, name='adminapproval'),

    url(r'^prioritization$', views.prioritization, name='prioritization'),
    url(r'^post_prioritization', views.post_prioritization, name='post_prioritization'),

    url(r'^acceptance$', views.acceptance, name='acceptance'),
    url(r'^post_acceptance$', views.post_acceptance, name='post_acceptance'),

    url(r'^dashboard$', views.dashboard, name='dashboard'),
    url(r'^dashboard_edit$', views.dashboard_edit, name='dashboard_edit'),

    url(r'^reports$', views.reports, name='reports'),
    url(r'^nok$', views.nok, name='nok'),
    url('', views.loginuser, name='loginuser'),


    # path('proxyreg/', views.proxyReg, name='proxyreg'),

]