from django import forms
from .models import WebUser


class WebUserAdminForm(forms.ModelForm):
    username = forms.CharField(
        widget=forms.TextInput(attrs={"class": "form-control",
                                      "placeholder": "Enter your user name here."}))
    password = forms.CharField(
        widget=forms.PasswordInput(attrs={"class": "form-control",
                                          "placeholder": "Enter your password here."}))

    class Meta:
        model = WebUser
        fields = '__all__'


class WebUserLoginForm(forms.ModelForm):
    username = forms.CharField(
        widget=forms.TextInput(attrs={"class": "form-control",
                                      "placeholder": "Enter your user name here."}))
    password = forms.CharField(
        widget=forms.PasswordInput(attrs={"class": "form-control",
                                          "placeholder": "Enter your password here."}))

    class Meta:
        model = WebUser
        fields = ('username','password')

    # def clean_username(self):

        # usernames = []
        # for each_user in User.objects.all():
            # usernames.append(each_user.username)

        # clean_username = self.cleaned_data['username']
        # if clean_username not in usernames:
            # raise forms.ValidationError("User Doesnt Exist")

        # Always return a value to use as the new cleaned data, even if
        # this method didn't change it.
        # return clean_username



    # def clean_username(self):
    #
    #     data = self.cleaned_data['username']
    #     if "fred@example.com" not in data:
    #         raise forms.ValidationError("You have forgotten about Fred!")
    #
    #     # Always return a value to use as the new cleaned data, even if
    #     # this method didn't change it.
    #     return data
    #
    # def clean(self):
    #     cleaned_data = super().clean()
    #     cc_myself = cleaned_data.get("cc_myself")
    #     subject = cleaned_data.get("subject")
    #
    #     if cc_myself and subject and "help" not in subject:
    #         msg = "Must put 'help' in subject when cc'ing yourself."
    #         self.add_error('cc_myself', msg)
    #         self.add_error('subject', msg)
